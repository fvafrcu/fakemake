Dear CRAN Team,
this is a resubmission of package 'fakemake'. I have added the following changes:

* Fixed CRAN notes on Escaped LaTeX specials.

Please upload to CRAN.
Best, Andreas Dominik

# Package fakemake 1.11.1

Reporting is done by packager version 1.15.2


## Test environments
- R Under development (unstable) (2023-07-29 r84787)
   Platform: x86_64-pc-linux-gnu
   Running under: Devuan GNU/Linux 4 (chimaera)
   0 errors | 0 warnings | 1 note 
- win-builder (devel)  
  Date: Tue, 15 Aug 2023 23:26:12 +0200
  Your package fakemake_1.11.1.tar.gz has been built (if working) and checked for Windows.
  Please check the log files and (if working) the binary package at:
  https://win-builder.r-project.org/Q7lL6X26DYaA
  The files will be removed after roughly 72 hours.
  Installation time in seconds: 4
  Check time in seconds: 65
  Status: OK
  R version 4.3.1 (2023-06-16 ucrt)
   

## Local test results
- RUnit:
    fakemake_unit_test - 18 test functions, 0 errors, 0 failures in 40 checks.
- testthat:
    [ FAIL 0 | WARN 0 | SKIP 0 | PASS 1 ]
- Coverage by covr:
    fakemake Coverage: 88.55%

## Local meta results
- Cyclocomp:
     Exceeding maximum cyclomatic complexity of 10 for make by 15.
     Exceeding maximum cyclomatic complexity of 10 for read_makefile by 5.
- lintr:
    found 22 lints in 663 lines of code (a ratio of 0.0332).
- cleanr:
    found 5 dreadful things about your code.
- codetools::checkUsagePackage:
    found 9 issues.
- devtools::spell_check:
    found 36 unkown words.
